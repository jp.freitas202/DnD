import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import jsSHA from 'jssha';
import { Storage } from '@ionic/storage';

/*
  Generated class for the AccessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccessProvider {
  url:string = 'http://br428.teste.website/~brisa293/dnd/handler.php';
  data:any;
  prevarray:any;
  uKey:any;
  constructor(public http: HttpClient, private stor: Storage) {
    console.log('Hello AccessProvider Provider');
  }
  
  authFunc(username, password){
    console.log('Função de login sumonada com sucesso!');
    this.prevarray = password+username;
    let shaObj = new jsSHA("SHA-256", "TEXT");
    shaObj.update(this.prevarray);
    this.uKey = shaObj.getHash("HEX");
    return this.http.get(this.url+'?key='+this.uKey+'&uname='+username+'&func=auth')
      .map(res => {
        this.data = res;
        return this.data;
      });
    }

    listFetch(username){
      return this.http.get(this.url+'?func=list&username='+username) .map(res => {
          console.log(this.url+'?func=list&username='+username)
          return res;
        })
    }
  }

