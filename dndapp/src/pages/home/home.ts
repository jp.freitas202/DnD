import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AccessProvider } from '../../providers/access/access';
import { PlayerlistPage } from '../playerlist/playerlist';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userName:string; //Variável que armazena o username digitado
  password:string; //Variável que armazena a senha digitada
  answer: any; //Variavel que aramazena resposta do servidor]
  errordump:any;

  constructor(public navCtrl: NavController, public access: AccessProvider, private stor: Storage) {
    this.answer = 0;
  }
  login(){
    this.access.authFunc(this.userName, this.password) .subscribe(data => {
      this.answer = data;
      console.log(this.answer)
      if(this.answer.status == 'success'){
        this.stor.set('uname', this.userName);
        this.navCtrl.push(PlayerlistPage);
      }else{
        this.errordump = 1;
      }
    });

  }

}
