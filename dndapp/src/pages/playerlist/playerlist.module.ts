import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlayerlistPage } from './playerlist';

@NgModule({
  declarations: [
    PlayerlistPage,
  ],
  imports: [
    IonicPageModule.forChild(PlayerlistPage),
  ],
})
export class PlayerlistPageModule {}
