import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AccessProvider } from '../../providers/access/access';

/**
 * Generated class for the PlayerlistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playerlist',
  templateUrl: 'playerlist.html',
})
export class PlayerlistPage {

  userName: any;
  loadstatus: any = 0;
  dump: any;
  d2: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private stor: Storage, public ac: AccessProvider) {
    this.stor.get('uname').then(result => {
      this.userName = result;
      this.dump = this.ac.listFetch(this.userName) .subscribe(data => {
        console.log('constructor')
        console.log(data);
        return data;
      });
      this.loadstatus = 1;
    });
  }


  ionViewDidLoad() {
  }

}
