# Mapa dos links da planilha.
#### Documento que descreve quais locais estão relacionados.

## Dados do personagem:
#### **Nome do personagem, Nome do jogador, Classe, Raça, Tendência, Região de Origem, Religião, Cor dos olhos, Descrição:**
 - Campos alteráveis que aceitam strings simples (utf-8)

#### **Nível, Idade, Peso, Altura**
 - Campos alteráveis que aceitam números intgers simples
*__Observação:__* Se altura se mantiver 'integer' deve ser descrito em centímetros

## Habilidades:
#### **Força, Destreza, Constituição, Inteligência, Sabedoria e Carisma:**
  - O campo _valor_ é alterável e permite qualquer valor do tipo 'integer';
  - O campo _modificador de habilidade_ é não alterável e tem seu valor definido de acordo com o campo _valor_, regido pela equ _'valor - 10 / 2'_;
  - O campo _valor temporário_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificador temporário_ é não alterável e funciona como o campo _modificador de habilidade_;

#### **Iniciativa:**
  - O campo _total_ é não alterável e tem seu valor definido pela soma do _modificador de habilidade_ de **Destreza** na área de Habilidades e do campo _outros_;
  - O campo _modificador de destreza_ é não alterável e seu valor e definido pelo valor de _modificador de habilidade_ de **Destreza** na área de Habilidades;
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Aparência:**
  - O campo _total_ é não alterável e tem seu valor definido pela soma do _modificador de habilidade_ de **Destreza** na área de Habilidades e do campo _valor_;
  - O campo _modificador de carisma_ é não alterável e seu valor e definido pelo valor de _modificador de habilidade_ de **Carisma** na área de Habilidades;
  - O campo _valor_ é alterável e aceita qualquer valor 'integer';

#### **Base de ataque:**
  - O _1º_ campo é alterável e aceita qualquer valor 'integer';
  - O _2º_ campo é alterável e aceita qualquer valor 'integer';
  - O _3º_ campo é alterável e aceita qualquer valor 'integer';
  - O _4º_ campo é alterável e aceita qualquer valor 'integer';


## Teste de resistência:
#### **Fortitude:**
  - O campo _total_ é não alterável e seu valor é baseado na soma do valor dos campos _base, modificador de habilidade, modificador de talento, outros modificadores e modificadores temporários_.
  - O campo _base_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificador de habilidade_ é não alterável e seu valor e definido pelo valor de _modificador de habilidade & modificador temporário*_ de **Constituição** da área de Habilidades;
  - O campo _modificador de talento_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros modificadores_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificadores temporários_ é alterável e aceita qualquer valor 'integer';

#### **Reflexos:**
  - O campo _total_ é não alterável e seu valor é baseado na soma do valor dos campos _base, modificador de habilidade, modificador de talento, outros modificadores e modificadores temporários_.
  - O campo _base_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificador de habilidade_ é não alterável e seu valor e definido pelo valor de _modificador de habilidade & modificador temporário*_ de **Destreza** da área de Habilidades;
  - O campo _modificador de talento_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros modificadores_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificadores temporários_ é alterável e aceita qualquer valor 'integer';

#### **Vontade:**
  - O campo _total_ é não alterável e seu valor é baseado na soma do valor dos campos _base, modificador de habilidade, modificador de talento, outros modificadores e modificadores temporários_.
  - O campo _base_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificador de habilidade_ é não alterável e seu valor e definido pelo valor de _modificador de habilidade & modificador temporário*_ de **Sabedoria** da área de Habilidades;
  - O campo _modificador de talento_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros modificadores_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificadores temporários_ é alterável e aceita qualquer valor 'integer';

#### **Corpo a corpo:**
  - O campo _total_ é não alterável e seus valores são definidos por:
      - _1º_ = _1ª_ de **Base de ataque** + _Bônus de força_ de **corpo a corpo** + _Bônus de tamanho_ de **corpo a corpo** + _Outros modificadores_ de **corpo a corpo** + _modificador temporário_ de **corpo a corpo**;
      - _2º_ = _2ª_ de **Base de ataque** + _Bônus de força_ de **corpo a corpo** + _Bônus de tamanho_ de **corpo a corpo** + _Outros modificadores_ de **corpo a corpo** + _modificador temporário_ de **corpo a corpo**;
      - _3º_ = _3ª_ de **Base de ataque** + _Bônus de força_ de **corpo a corpo** + _Bônus de tamanho_ de **corpo a corpo** + _Outros modificadores_ de **corpo a corpo** + _modificador temporário_ de **corpo a corpo**;
      - _4º_ = _4ª_ de **Base de ataque** + _Bônus de força_ de **corpo a corpo** + _Bônus de tamanho_ de **corpo a corpo** +   _Outros modificadores_ de **corpo a corpo** + _modificador temporário_ de **corpo a corpo**;

  - O campo _base de ataque_ tem seus valores idênticos aos valores de _base de ataque_ em **Habilidades**;
  - O campo _Bônus de força_ é não aterável e seu valor é definido pelo _modificador de habilidade_ de **Força**;
  - O campo _Bônus de tamanho_ é alterável e aceita qualquer valor 'integer';
  - O campo _Outros modificadores_ é alterável e aceita qualquer valor 'integer';
  - O campo _modificador temporário_ é alterável e aceita qualquer valor 'integer';

## Armas, armadura e escudo/proteção:
#### **Arma (x4):**
  - O campo _arma_ é alterável e aceita strings
  - O campo _bônus de ataque_ é alterável e aceita qualquer valor 'integer';
  - O campo _dano_ é alterável e aceita qualquer valor 'integer';
  - O campo _decisivo_ é alterável e aceita qualquer valor 'integer';
  - O campo _alcance_ é alterável e aceita qualquer valor 'integer';
  - O campo _peso_ é alterável e aceita qualquer valor 'integer';
  - O campo _tipo_ é alterável e aceita strings
  - O campo _tamanho_ é alterável e aceita qualquer valor 'integer';
  - O campo _propriedades especiais_ é alterável e aceita strings;

#### **Armadura:**
  - O campo _armadura_ é alterável e aceita strings
  - O campo _tipo_ é alterável e aceita sstring;
  - O campo _bônus armadura_ é alterável e aceita qualquer valor 'integer';
  - O campo _bônus destreza_ é alterável e aceita qualquer valor 'integer';
  - O campo _% falhar magia_ é alterável e aceita qualquer valor 'integer' entre 0 e 100;
  - O campo _penalidade_ é alterável e aceita qualquer valor 'integer'; (?)
  - O campo _peso_ é alterável e aceita qualquer valor 'integer';
  - O campo _deslocamento_ é alterável e aceita qualquer valor 'integer';
  - O campo _propriedades especiais_ é alterável e aceita strings;

#### **Classe de armadura**
  - O campo total é não alterável e seu valor é definido pela equ: 10 + _Bônus de armadura_ + _Bônus de escudo(?)_ + _modificador de habilidade_ de **Destreza** + _tamanho_ + _natural_ + _outros_;
  - O campo _tamanho_ é alterável e aceita qualquer integer positivo;
  - O campo _Natural_ é alterável e aceita qualquer integer;
  - O campo _Outros_ é alterável e aceita qualquer integer;
  - O campo _CA para toque_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _tamanho_ + _natural_ + _outros_ + 10;
  - O campo _CA surpresa_ é não alterável e seu valor é definido pela equ: _Bônus de armadura_ + _Bônus de escudo_ + _tamanho_ + _natural_ + _outros_;
  - O campo _chance de magia falhar_ é não alterável e seu valor é definido pela equ: (?);
  - O campo _red. de dano_ é alterável e aceita qualquer valor 'integer';
  - O campo _Penalidade por uso de armadura_ é não alterável e seu valor é definido pela equ: _Penalidade_ de **Armadura** + _Penalidade_ de **Escudo/Proteção**;

#### **Escudo/Proteção:**
  - O campo _escudo/proteção_ é alterável e aceita strings
  - O campo _tipo_ é alterável e aceita sstring;
  - O campo _bônus armadura_ é alterável e aceita qualquer valor 'integer';
  - O campo _bônus destreza_ é alterável e aceita qualquer valor 'integer';
  - O campo _% falhar magia_ é alterável e aceita qualquer valor 'integer' entre 0 e 100;
  - O campo _penalidade_ é alterável e aceita qualquer valor 'integer'; (?)
  - O campo _peso_ é alterável e aceita qualquer valor 'integer';
  - O campo _deslocamento_ é alterável e aceita qualquer valor 'integer';
  - O campo _propriedades especiais_ é alterável e aceita strings;

#### **Munição:**
 **Requer mais detalhes.**

#### **Outros:**
  **Requer mais detalhes.**

## Perícias:
#### **Abrir fechaduras:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Acrobacia:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Adestrar animais:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Alquimia:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Arte da fuga:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Atuação (x3):**
  - O campo _atuação_ é alterável e aceita qualquer string;
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Avaliação:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Blefar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Cavalgar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Concentração:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Constituição** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Constituição**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento:**
  - O campo _conhecimento_ é alterável e aceita qualquer string;
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento arcano:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento geográfico:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento Histórico:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento natural:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento Nobreza:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento planar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Conhecimento religioso:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Cura:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Decifrar escrita:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Diplomacia:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Disfarce:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Equilíbrio:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Escalar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Força** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Força**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Esconder-se:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Falsificação:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Furtividade:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Identificar magia:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Intimidar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

####  **Ler e escrever:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é nulo;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Natação:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Força** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Força**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Observar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Obter Informações:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Ofícios (x3):**
  - O campo _Ofício_ é alterável e aceita qualquer string;
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Operar mecanismo:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Ouvir:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Procurar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Inteligência** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Inteligência**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Profissão (x2):**
  - O campo _profissão_ é alterável e aceita qualquer string;
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Prestigitação:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Saltar:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Força** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Força**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Sentir motivação:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Sobrevivência:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Sabedoria** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Sabedoria**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Usar cordas:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Destreza** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Destreza**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

#### **Usar instrumento mágico:**
  - O campo _bônus total_ é não alterável e seu valor é definido pela equ: _modificador de habilidade_ de **Carisma** + _Graduação_ + _Outros_;
  - O campo _Modificador_ é não alterável e seu valor é definido pelo _modificador de habilidade_ de **Carisma**;
  - O campo _graduação_ é alterável e aceita qualquer valor 'integer';
  - O campo _outros_ é alterável e aceita qualquer valor 'integer';

## Deslocamento:
#### **Racial:**
 - Campo alterável que aceita qualquer 'integer';

#### **Básico:**
 - Campo não alterável que tem seu valor definido por _Deslocamento_ de **Armadura** (se existente) ou por **Deslocamento Racial** (caso não haja armadura ou valor de Deslocamento da armadura);

#### **Quadrados¹:**
  - Campo não alterável que tem seu valor definido por:

```
  se('Deslocamento de Armadura' / 1,5 > 0):
    'Quadrados' = 'Deslocamento de Armadura' / 1,5;
  caso não:
    'Quadrados' = 'nulo';

  se(!'Deslocamento de Armadura'):
    se('Deslocamento básico' / 1,5 > 0):
      'Quadrados' = 'Deslocamento básico' / 1,5;
    caso não:
      'Quadrados' = 'nulo';
```

#### **Corrida:**
  - Campo não alterável que tem seu valor definido por:

```
se('Deslocamento de armadura' * 4 > 0):
  'corrida' = 'Deslocamento de armadura' * 4;
caso não:
  'corrida' = nulo;

se(!'Deslocamento de armadura'):
  se('Deslocamento básico' * 4 > 0):
    'corrida' = 'Deslocamento básico' * 4;
  caso não:
    'corrida' = nulo;
```

#### **Quadrados²:**
  - Campo não alterável que tem seu valor definido por:

```
  se('Deslocamento de armadura' * 4 / 1,5 > 0):
    'corrida' = 'Deslocamento de armadura' * 4 / 1,5;
  caso não:
    'corrida' = nulo;

  se(!'Deslocamento de armadura'):
    se('Deslocamento básico' * 4 / 1,5 > 0):
      'corrida' = 'Deslocamento básico * 4 / 1,5;
    caso não:
      'corrida' = nulo;
```

## **Locais**
Campo | Local |
------|-------
_valor_ de **Força** | H13
_valor_ de **Destreza** | H17
_valor_ de **Constituição** | H21
_valor_ de **Inteligência** | H25
_valor_ de **Sabedoria** | H29
_valor_ de **Carisma** | H33
_modificador de habilidade_ de **Força** | M13
_modificador de habilidade_ de **Destreza** | M17
_modificador de habilidade_ de **Constituição** | M21
_modificador de habilidade_ de **Inteligência** | M25
_modificador de habilidade_ de **Sabedoria** | M29
_modificador de habilidade_ de **Carisma** | M33
_valor temporário_ de **Força** | R13
_valor temporário_ de **Destreza** | R17
_valor temporário_ de **Constituição** | R21
_valor temporário_ de **Inteligência** | R25
_valor temporário_ de **Sabedoria** | R29
_valor temporário_ de **Carisma** | R33
_modificador temporário_ de **Força** | W13
_modificador temporário_ de **Destreza** | W17
_modificador temporário_ de **Constituição** | W21
_modificador temporário_ de **Inteligência** | W25
_modificador temporário_ de **Sabedoria** | W29
_modificador temporário_ de **Carisma** | W33
_total_ de **Iniciativa** | AO23
_outros_ de **Iniciativa** | AY23
_total_ de **Aparência** | AO28
_valor_ de **Aparência** | AY28
_total_ de **Fortitude** | M39
_base_ de **Fortitude** | R39
_modificador de talento_ de **Fortitude** | AB39
_outros modificadores_ de **Fortitude** | AG39
_modificador temporário_ de **Fortitude** | AL39
_total_ de **Reflexos** | M43
_base_ de **Reflexos** | R43
_modificador de talento_ de **Reflexos** | AB43
_outros modificadores_ de **Reflexos** | AG43
_modificador temporário_ de **Reflexos** | AL43
_total_ de **Vontade** | M47
_base_ de **Vontade** | R47
_modificador de talento_ de **Vontade** | AB47
_outros modificadores_ de **Vontade** | AG47
_modificador temporário_ de **Vontade** | AL47
_1º_ de **Base de ataque** | AT33
_2º_ de **Base de ataque** | AW33
_3º_ de **Base de ataque** | AY33
_4º_ de **Base de ataque** | BA33
_1º_ de total em **Corpo a corpo** | P54
_2º_ de total em **Corpo a corpo** | S54
_3º_ de total em **Corpo a corpo** | U54
_4º_ de total em **Corpo a corpo** | W54
_1º_ de base de ataque em **Corpo a corpo** | Z54
_2º_ de base de ataque em **Corpo a corpo** | AC54
_3º_ de base de ataque em **Corpo a corpo** | AE54
_4º_ de base de ataque em **Corpo a corpo** | AG54
_Arma_ de **Armas, armadura e escudo/proteção** | A66, A76, A86, A96
_Bônus de ataque_ de **Armas, armadura e escudo/proteção** | T66, T76, T86, T96
_Dano_ de **Armas, armadura e escudo/proteção** | AL66, AL76, AL86, AL96
_Decisivo_ de **Armas, armadura e escudo/proteção** | AU66, AU76, AU86, AU96
_Alcance_ de **Armas, armadura e escudo/proteção** | A70, A80, A90, A100
_Peso_ de arma em **Armas, armadura e escudo/proteção** | H70, H80, H90, H100
_Tipo_ de arma em **Armas, armadura e escudo/proteção** | N70, N80, N90, N100
_Tamanho_ de **Armas, armadura e escudo/proteção** | S70, S80, S90, S100
_Propriedades especiais_ de arma em **Armas, armadura e escudo/proteção** | Z70, Z80, Z90, Z100
_Armadura_ **Armas, armadura e escudo/proteção** | A106
_Tipo_ de armadura em **Armas, armadura e escudo/proteção** | T106
_Bônus de armadura_ **Armas, armadura e escudo/proteção** | Z106
_Bônus de destreza_ **Armas, armadura e escudo/proteção** | AJ106
_% falhar magia_ **Armas, armadura e escudo/proteção** | AT106
_Penalidade_ **Armas, armadura e escudo/proteção** | A110
_Peso_ de armadura em **Armas, armadura e escudo/proteção** | I110
_Deslocamento_ **Armas, armadura e escudo/proteção** | N110
_Propriedade especiais_ de armadura em **Armas, armadura e escudo/proteção** | Y110
_Escudo/Proteção_ **Armas, armadura e escudo/proteção** | A116
_Tipo_ de armadura em **Armas, armadura e escudo/proteção** | T116
_Bônus de armadura_ **Armas, armadura e escudo/proteção** | Z116
_Bônus de destreza_ **Armas, armadura e escudo/proteção** | AJ116
_% falhar magia_ **Armas, armadura e escudo/proteção** | AT116
_Penalidade_ **Armas, armadura e escudo/proteção** | A120
_Peso_ de armadura em **Armas, armadura e escudo/proteção** | I120
_Deslocamento_ **Armas, armadura e escudo/proteção** | N120
_Propriedade especiais_ de armadura em **Armas, armadura e escudo/proteção** | Y120
_Total_ de **Classe de armadura** | AO17
_Armadura_ de **Classe de armadura** | AT17
_Escudo_ de **Classe de armadura** | AY17
_Destreza_ de **Classe de armadura** | BD17
_Tamanho_ de **Classe de armadura** | BI17
_Natural_ de **Classe de armadura** | BN17
_Outros_ de **Classe de armadura** | BS17
_CA para toque_ de **Classe de armadura** | BX17
_CA surpresa_ de **Classe de armadura** | CC17
_Chance de magia falhar_ de **Classe de armadura** | CH17
_Red. de dano_ de **Classe de armadura** | CM17
_Penalidade por uso de armadura_ de **Classe de armadura** | CR17
_Bônus total_ de **Abrir fechaduras** | CC29
_Modificador_ de **Abrir fechaduras** | CH29
_Graduação_ de **Abrir fechaduras** | CM29
_Outros_ de **Abrir fechaduras** | CR29
_Bônus total_ de **Acrobacia** | CC31
_Modificador_ de **Acrobacia** | CH31
_Graduação_ de **Acrobacia** | CM31
_Outros_ de **Acrobacia** | CR31
_Bônus total_ de **Adestrar animais** | CC33
_Modificador_ de **Adestrar animais** | CH33
_Graduação_ de **Adestrar animais** | CM33
_Outros_ de **Adestrar animais** | CR33
_Bônus total_ de **Alquimia** | CC35
_Modificador_ de **Alquimia** | CH35
_Graduação_ de **Alquimia** | CM35
_Outros_ de **Alquimia** | CR35
_Bônus total_ de **Arte da fuga** | CC37
_Modificador_ de **Arte da fuga** | CH37
_Graduação_ de **Arte da fuga** | CM37
_Outros_ de **Arte da fuga** | CR37
_atuação_ de **Atuação** | BO39, BO41, BO43
_Bônus total_ de **Atuação** | CC39, CC41, CC43
_Modificador_ de **Atuação** | CH39, CH41, CH43
_Graduação_ de **Atuação** | CM39, CH41, CH43
_Outros_ de **Atuação** | CR39, CR41, CR43
_Bônus total_ de **Avaliação** | CC45
_Modificador_ de **Avaliação** | CH45
_Graduação_ de **Avaliação** | CM45
_Outros_ de **Avalição** | CR45
_Bônus total_ de **Blefar** | CC47
_Modificador_ de **Blefar** | CH47
_Graduação_ de **Blefar** | CM47
_Outros_ de **Blefar** | CR47
_Bônus total_ de **Cavalgar** | CC49
_Modificador_ de **Cavalgar** | CH49
_Graduação_ de **Cavalgar** | CM49
_Outros_ de **Cavalgar** | CR49
_Bônus total_ de **Concentração** | CC51
_Modificador_ de **Concentração** | CH51
_Graduação_ de **Concentração** | CM51
_Outros_ de **Concentração** | CR51
_conhecimento_ de **Conhecimento** | BR53
_Bônus total_ de **Conhecimento** | CC53
_Modificador_ de **Conhecimento** | CH53
_Graduação_ de **Conhecimento** | CM53
_Outros_ de **Conhecimento** | CR53
_Bônus total_ de **Conhecimento arcano** | CC55
_Modificador_ de **Conhecimento arcano** | CH55
_Graduação_ de **Conhecimento arcano** | CM55
_Outros_ de **Conhecimento arcano** | CR55
_Bônus total_ de **Conhecimento geográfico** | CC57
_Modificador_ de **Conhecimento geográfico** | CH57
_Graduação_ de **Conhecimento geográfico** | CM57
_Outros_ de **Conhecimento geográfico** | CR57
_Bônus total_ de **Conhecimento Histórico** | CC59
_Modificador_ de **Conhecimento Histórico** | CH59
_Graduação_ de **Conhecimento Histórico** | CM59
_Outros_ de **Conhecimento Histórico** | CR59
_Bônus total_ de **Conhecimento Natural** | CC61
_Modificador_ de **Conhecimento Natural** | CH61
_Graduação_ de **Conhecimento Natural** | CM61
_Outros_ de **Conhecimento Natural** | CR61
_Bônus total_ de **Conhecimento Nobreza** | CC63
_Modificador_ de **Conhecimento Nobreza** | CH63
_Graduação_ de **Conhecimento Nobreza** | CM63
_Outros_ de **Conhecimento Nobreza** | CR63
_Bônus total_ de **Conhecimento Planar** | CC65
_Modificador_ de **Conhecimento Planar** | CH65
_Graduação_ de **Conhecimento Planar** | CM65
_Outros_ de **Conhecimento Planar** | CR65
_Bônus total_ de **Conhecimento Religioso** | CC67
_Modificador_ de **Conhecimento Religioso** | CH67
_Graduação_ de **Conhecimento Religioso** | CM67
_Outros_ de **Conhecimento Religioso** | CR67
_Bônus total_ de **Cura** | CC69
_Modificador_ de **Cura** | CH69
_Graduação_ de **Cura** | CM69
_Outros_ de **Cura** | CR69
_Bônus total_ de **Decifrar escrita** | CC71
_Modificador_ de **Decifrar escrita** | CH71
_Graduação_ de **Decifrar escrita** | CM71
_Outros_ de **Decifrar escrita** | CR71
_Bônus total_ de **Diplomacia** | CC73
_Modificador_ de **Diplomacia** | CH73
_Graduação_ de **Diplomacia** | CM73
_Outros_ de **Diplomacia** | CR73
_Bônus total_ de **Disfarce** | CC75
_Modificador_ de **Disfarce** | CH75
_Graduação_ de **Disfarce** | CM75
_Outros_ de **Disfarce** | CR75
_Bônus total_ de **Equilíbrio** | CC77
_Modificador_ de **Equilíbrio** | CH77
_Graduação_ de **Equilíbrio** | CM77
_Outros_ de **Equilíbrio** | CR77
_Bônus total_ de **Escalar** | CC79
_Modificador_ de **Escalar** | CH79
_Graduação_ de **Escalar** | CM79
_Outros_ de **Escalar** | CR79
_Bônus total_ de **Esconder-se** | CC81
_Modificador_ de **Esconder-se** | CH81
_Graduação_ de **Esconder-se** | CM81
_Outros_ de **Esconder-se** | CR81
_Bônus total_ de **Falsificação** | CC83
_Modificador_ de **Falsificação** | CH83
_Graduação_ de **Falsificação** | CM83
_Outros_ de **Falsificação** | CR83
_Bônus total_ de **Furtividade** | CC85
_Modificador_ de **Furtividade** | CH85
_Graduação_ de **Furtividade** | CM85
_Outros_ de **Furtividade** | CR85
_Bônus total_ de **Identificar magia** | CC87
_Modificador_ de **Identificar magia** | CH87
_Graduação_ de **Identificar magia** | CM87
_Outros_ de **Identificar magia** | CR87
_Bônus total_ de **Intimidar** | CC89
_Modificador_ de **Intimidar** | CH89
_Graduação_ de **Intimidar** | CM89
_Outros_ de **Intimidar** | CR89
_Bônus total_ de **Ler e escrever** | CC91
_Modificador_ de **Ler e escrever** | CH91
_Graduação_ de **Ler e escrever** | CM91
_Outros_ de **Ler e escrever** | CR91
_Bônus total_ de **Natação** | CC93
_Modificador_ de **Natação** | CH93
_Graduação_ de **Natação** | CM93
_Outros_ de **Natação** | CR93
_Bônus total_ de **Observar** | CC95
_Modificador_ de **Observar** | CH95
_Graduação_ de **Observar** | CM95
_Outros_ de **Observar** | CR95
_Bônus total_ de **Obter Informações** | CC97
_Modificador_ de **Obter Informações** | CH97
_Graduação_ de **Obter Informações** | CM97
_Outros_ de **Obter Informações** | CR97
_ofícios_ de **Ofícios** | BC99, BC101, BC103
_Bônus total_ de **Ofícios** | CC99, CC101, CC103
_Modificador_ de **Ofícios** | CH99, CH101, CC103
_Graduação_ de **Ofícios** | CM99, CM101, CM103
_Outros_ de **Ofícios** | CR99, CR101, CM103
_Bônus total_ de **Operar mecanismo** | CC105
_Modificador_ de **Operar mecanismo** | CH105
_Graduação_ de **Operar mecanismo** | CM105
_Outros_ de **Operar mecanismo** | CR105
_Bônus total_ de **Ouvir** | CC107
_Modificador_ de **Ouvir** | CH107
_Graduação_ de **Ouvir** | CM107
_Outros_ de **Ouvir** | CR107
_Bônus total_ de **Procurar** | CC109
_Modificador_ de **Procurar** | CH109
_Graduação_ de **Procurar** | CM109
_Outros_ de **Procurar** | CR109
_profissão_ de **Profissão** | BC111, BC113
_Bônus total_ de **Profissão** | CC111, CC113
_Modificador_ de **Profissão** | CH111, CH113
_Graduação_ de **Profissão** | CM111, CM113
_Outros_ de **Profissão** | CR111, CR113
_Bônus total_ de **Prestigitação** | CC115
_Modificador_ de **Prestigitação** | CH115
_Graduação_ de **Prestigitação** | CM115
_Outros_ de **Prestigitação** | CR115
_Bônus total_ de **Saltar** | CC117
_Modificador_ de **Saltar** | CH117
_Graduação_ de **Saltar** | CM117
_Outros_ de **Saltar** | CR117
_Bônus total_ de **Sentir motivação** | CC119
_Modificador_ de **Sentir motivação** | CH119
_Graduação_ de **Sentir motivação** | CM119
_Outros_ de **Sentir motivação** | CR119
_Bônus total_ de **Sobrevivência** | CC121
_Modificador_ de **Sobrevivência** | CH121
_Graduação_ de **Sobrevivência** | CM121
_Outros_ de **Sobrevivência** | CR121
_Bônus total_ de **Usar cordas** | CC123
_Modificador_ de **Usar cordas** | CH123
_Graduação_ de **Usar cordas** | CM123
_Outros_ de **Usar cordas** | CR123
_Bônus total_ de **Usar instrumento mágico** | CC125
_Modificador_ de **Usar instrumento mágico** | CH125
_Graduação_ de **Usar instrumento mágico** | CM125
_Outros_ de **Usar instrumento mágico** | CR125
_Racial_ de **Deslocamento** | CD13
_Básico_ de **Deslocamento** | CH13
_Quadrados¹_ de **Deslocamento** | CK13
_Corrida_ de **Deslocamento** | CO13
_Quadrados²_ de **Deslocamento** | CR13

## Notas:
 - Na planilha o campo _Chance de magia falhar_ está buscando um campo não definido (AT1163) e resulta em porcentagens não proporcionais
 - _Bônus de armadura_ é o mesmo valor em **armadura** e **escudo/proteção**?
 - A equação de _% chance de magia falhar_ está quebrada :/
